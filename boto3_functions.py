import boto3
import logging
from urllib3.exceptions import NewConnectionError
from botocore.exceptions import ClientError
from botocore.exceptions import EndpointConnectionError
from botocore.exceptions import ParamValidationError


class boto3Functions:

    s3 = boto3.client('s3')
    s3_resource = boto3.resource("s3")

    # def __init__(self):
    #     self.bucket_name =

    def list_buckets(self):
        response = self.s3.list_buckets()

        # Output the bucket names
        print('Existing buckets:')
        for bucket in response['Buckets']:
            print(f'  {bucket["Name"]}')

    def create_bucket(self):
      """Create an S3 bucket in a specified region

      If a region is not specified, the bucket is created in the S3 default
      region (us-east-1).

      :param bucket_name: Bucket to create
      :param region: String region to create bucket in
      :return: True if bucket created, else False
      """
      region = input('What region would you like to create your bucket in?  ')
      if region == '':
          region = None
      bucket = input('What is the name of your bucket?  ')
      # Create bucket
      try:
          if region is None:
              self.s3.create_bucket(Bucket=bucket)
          else:
              s3_client = boto3.client('s3', region_name=region)
              location = {'LocationConstraint': region}
              s3_client.create_bucket(Bucket=bucket,
              CreateBucketConfiguration=location)
      except ClientError as e:
         logging.error(e)
         return False
      except EndpointConnectionError as err:
         logging.error(err)
         print('Invalid region. Please again')
         return False
      except ParamValidationError as p_err:
         logging.error(p_err)
         print('Invalid bucket name. Please try again')
         return False
      return True

    def delete_bucket(self):
        bucket_name = input('What is the name of the bucket you would like to delete?  ')
        try:
            response = self.s3.delete_bucket(
                Bucket=bucket_name,
            )

            print(response)
            if type(response) == dict:
                 return f'The bucket, {bucket_name}, has been successfully deleted'
            else:
                 return 'Something went wrong. Please try again'


    def upload_file(self):
        """Upload a file to an S3 bucket

        :param file_name: File to upload
        :param bucket: Bucket to upload to
        :param object_name: S3 object name. If not specified then file_name is used
        :return: True if file was uploaded, else False
        """
        file_name = input('What is the name of the file you want to upload?  ')
        bucket = input('What is the name of the bucket the file will be uploaded to?  ')
        object_name = input('What is the new name of the file? if it\'s the same, leave blank:  ')
        # If S3 object_name was not specified, use file_name
        if object_name == '':
            object_name = file_name

        # Upload the file
        try:
            response = self.s3.upload_file(file_name, bucket, object_name)
        except ClientError as e:
            logging.error(e)
            return False
        return True

    def delete_file(self):
        file_name = str(input('What is the name of the file you want to delete?  '))
        bucket_name = str(input('What is the name of the bucket the file will be deleted from?  '))

        obj = self.s3_resource.Object(bucket_name, file_name)
        obj.delete()

    def get_file(self):
        object_name = str(input('What is the name of the file you want to retrieve?  '))
        bucket_name = str(input('What is the name of the bucket the file will be retrieved from?  '))
        file_name = str(input('What is the new name of the file? if it\'s the same, leave blank:  '))
        if object_name == '':
            file_name = object_name

        self.s3.download_file(bucket_name, object_name, file_name)

fun = boto3Functions()
