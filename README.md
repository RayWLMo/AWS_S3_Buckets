# Disaster Recovery Plan using AWS Buckets
![S3 Bucket Diagram](https://gitlab.com/RayWLMo/AWS_S3_Buckets/-/raw/main/images/S3_Bucket_Diagram.png)
## Setting up an new instance
- Set up a new instance following the instructions [here](https://gitlab.com/RayWLMo/2_Tier_App_Deployment)
- For Security Gates, only allow `SSH` and `HTTP` from `My IP`
- SSH into instance using the same instructions
## Configuring python in Ubuntu instance
- check python version - `python -V`
- if not Python 3 or higher, uninstall python - sudo apt remove python
- Then reinstall python 3 - `sudo apt install python3.9`
- Make sure its set in path and make default
 - python --version
 - sudo su
 - update-alternatives --install python /usr/bin/python3 1
 - Ctrl + D to leave superuser
 - check version - `python -V`

## Installing Pip
- `sudo apt install python3-pip`
- Check version - `pip3 -V`

## Installing AWSCLI
- `sudo pip install awscli --upgrade --user`
- Update PATH - `export PATH=~/.local/bin:$PATH`
- `source ~/.bashrc`
- Check AWS version - `aws -V`

## Configuring AWSCLI
- `aws configure`
- Enter details from .csv file
- Region `eu-west-1`
- Output `json`

## Checking correct configuration by checking bucket list
- `aws s3 ls`
- should list buckets

# AWS Buckets
## Using the awscli module
### Making a new bucket
- `aws s3 mb s3://bucketname --region eu-west-1`
- **Bucket naming syntax can not include special characters excluding '-'**

### Checking if bucket is created
- `aws s3 ls`
- Also can check on AWS website, by going to S3 page and searching for bucket

### Copying files to bucket
- `aws s3 cp test.txt s3://eng89raymondapp/`

### Copying files from bucket
- `aws s3 cp s3://bucketname/file_name new_file_name`

## Using the `boto3` python package to manage Buckets
### Showing Buckets
```python
# Retrieve the list of existing buckets
s3 = boto3.client('s3')
response = s3.list_buckets()

# Output the bucket names
print('Existing buckets:')
for bucket in response['Buckets']:
    print(f'  {bucket["Name"]}')
```

### Creating a Bucket
```python
import logging
import boto3
from botocore.exceptions import ClientError


def create_bucket(bucket_name, region=None):
  """Create an S3 bucket in a specified region

  If a region is not specified, the bucket is created in the S3 default
  region (us-east-1).

  :param bucket_name: Bucket to create
  :param region: String region to create bucket in
  :return: True if bucket created, else False
  """

  # Create bucket
  try:
      if region is None:
          s3_client = boto3.client('s3')
          s3_client.create_bucket(Bucket=bucket_name)
      else:
          s3_client = boto3.client('s3', region_name=region)
          location = {'LocationConstraint': region}
          s3_client.create_bucket(Bucket=bucket_name,
          CreateBucketConfiguration=location)
   except ClientError as e:
     logging.error(e)
     return False
   return True

region_loc = input('What is the region, where your bucket will located?  ')
bucketname = input('What is the name of your bucket?  ')

create_bucket(bucketname, region_loc)
```

### Deleting a bucket
```python
import boto3
import requests

client = boto3.client('s3')

bucket_name = input('What is the name of the bucket you would like to delete?  ')
response = client.delete_bucket(
    Bucket=bucket_name,
)

print(response)
if type(response) == dict:
  print(f'The bucket, {bucket_name}, has been successfully deleted')
```

### Uploading a file
```python
import logging
import boto3
from botocore.exceptions import ClientError


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

filename = input('What is the name of the file you want to upload?  ')
bucket_name = input('What is the name of the bucket the file will be uploaded to?  ')
new_filename = input('What is the new name of the file? if it\'s the same, type in the old name:  ')

upload_file(filename, bucket_name, new_filename)
```

### Deleting a file
```python
import boto3

s3 = boto3.resource("s3")

file_name = str(input('What is the name of the file you want to delete?  '))
bucket_name = str(input('What is the name of the bucket the file will be deleted from?  '))

obj = s3.Object(bucket_name, file_name)
obj.delete()
```

### Retrieving a file
```python
import boto3

s3 = boto3.client('s3')

object_name = str(input('What is the name of the file you want to retrieve?  '))
bucket_name = str(input('What is the name of the bucket the file will be retrieved from?  '))
file_name = str(input('What is the new name of the file? if it\'s the same, type in the old name:  '))

s3.download_file(bucket_name, object_name, file_name)
```

### Using OOP to have all functions in one file
- Using classes and methods, all the individual functions can be made into one file
```python
import boto3
import logging
from urllib3.exceptions import NewConnectionError
from botocore.exceptions import ClientError
from botocore.exceptions import EndpointConnectionError
from botocore.exceptions import ParamValidationError

class boto3Functions:

    s3 = boto3.client('s3')
    s3_resource = boto3.resource("s3")

    # def __init__(self):
    #     self.bucket_name =

    def list_buckets(self):
        response = self.s3.list_buckets()

        # Output the bucket names
        print('Existing buckets:')
        for bucket in response['Buckets']:
            print(f'  {bucket["Name"]}')

    def create_bucket(self):
      """Create an S3 bucket in a specified region

      If a region is not specified, the bucket is created in the S3 default
      region (us-east-1).

      :param bucket_name: Bucket to create
      :param region: String region to create bucket in
      :return: True if bucket created, else False
      """
      region = input('What region would you like to create your bucket in?  ')
      if region == '':
          region = None
      bucket = input('What is the name of your bucket?  ')
      # Create bucket
      try:
          if region is None:
              self.s3.create_bucket(Bucket=bucket)
          else:
              s3_client = boto3.client('s3', region_name=region)
              location = {'LocationConstraint': region}
              s3_client.create_bucket(Bucket=bucket,
              CreateBucketConfiguration=location)
      except ClientError as e:
         logging.error(e)
         return False
      except EndpointConnectionError as err:
         logging.error(err)
         print('Invalid region. Please again')
         return False
      except ParamValidationError as p_err:
         logging.error(p_err)
         print('Invalid bucket name. Please try again')
         return False
      return True

    def delete_bucket(self):
        bucket_name = input('What is the name of the bucket you would like to delete?  ')
        try:
            response = self.s3.delete_bucket(
                Bucket=bucket_name,
            )

            print(response)
            if type(response) == dict:
                 return f'The bucket, {bucket_name}, has been successfully deleted'
            else:
                 return 'Something went wrong. Please try again'


    def upload_file(self):
        """Upload a file to an S3 bucket

        :param file_name: File to upload
        :param bucket: Bucket to upload to
        :param object_name: S3 object name. If not specified then file_name is used
        :return: True if file was uploaded, else False
        """
        file_name = input('What is the name of the file you want to upload?  ')
        bucket = input('What is the name of the bucket the file will be uploaded to?  ')
        object_name = input('What is the new name of the file? if it\'s the same, leave blank:  ')
        # If S3 object_name was not specified, use file_name
        if object_name == '':
            object_name = file_name

        # Upload the file
        try:
            response = self.s3.upload_file(file_name, bucket, object_name)
        except ClientError as e:
            logging.error(e)
            return False
        return True

    def delete_file(self):
        file_name = str(input('What is the name of the file you want to delete?  '))
        bucket_name = str(input('What is the name of the bucket the file will be deleted from?  '))

        obj = self.s3_resource.Object(bucket_name, file_name)
        obj.delete()

    def get_file(self):
        object_name = str(input('What is the name of the file you want to retrieve?  '))
        bucket_name = str(input('What is the name of the bucket the file will be retrieved from?  '))
        file_name = str(input('What is the new name of the file? if it\'s the same, leave blank:  '))
        if object_name == '':
            file_name = object_name

        self.s3.download_file(bucket_name, object_name, file_name)

fun = boto3Functions()
```

To call the function we can use the following command:
```shell
python -c 'from boto3_functions import *; fun.function_name()'
#For example, Creating a new bucket:
python -c 'from boto3_functions import *; fun.create_bucket()'
```
